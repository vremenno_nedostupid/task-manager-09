package ru.fedun.tm;

import ru.fedun.tm.constant.ArgumentConst;
import ru.fedun.tm.model.TerminalCommand;
import ru.fedun.tm.repository.CommandRepository;
import ru.fedun.tm.utils.NumberUtil;

import java.util.Arrays;
import java.util.Scanner;

import static ru.fedun.tm.constant.TerminalConst.*;

public class Application {

    public static final CommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        displayWelcome();
        if (args.length != 0) {
            runWithArgs(args[0]);
        }
        runWithCommand();
    }

    private static void runWithCommand() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (true) {
            command = scanner.nextLine();
            switch (command) {
                case VERSION:
                    displayVersion();
                    break;
                case ABOUT:
                    displayAbout();
                    break;
                case HELP:
                    displayHelp();
                    break;
                case INFO:
                    displayInfo();
                    break;
                case COMMANDS:
                    displayCommands();
                    break;
                case ARGUMENTS:
                    displayArgs();
                    break;
                case EXIT:
                    exit();
                default:
                    displayError();
            }
        }
    }

    private static void runWithArgs(final String arg) {
        if (arg == null || arg.isEmpty()) {
            System.out.println("Empty command");
            return;
        }
        switch (arg) {
            case ArgumentConst.HELP:
                displayHelp();
                break;
            case ArgumentConst.ABOUT:
                displayAbout();
                break;
            case ArgumentConst.VERSION:
                displayVersion();
                break;
            case ArgumentConst.INFO:
                displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                displayArgs();
                break;
            case ArgumentConst.COMMANDS:
                displayCommands();
                break;
            default:
                displayError();
        }
    }

    private static void displayWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println();
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final TerminalCommand command : commands) {
            System.out.println(command);
        }
        System.out.println();
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.9");
        System.out.println();
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Alexander Fedun");
        System.out.println("sasha171998@gmail.com");
        System.out.println();
    }

    private static void displayCommands() {
        final String[] commands = COMMAND_REPOSITORY.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    private static void displayArgs() {
        final String[] args = COMMAND_REPOSITORY.getArgs();
        System.out.println(Arrays.toString(args));
    }

    private static void displayInfo() {
        System.out.println("[INFO]");

        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + processors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory: " + usedMemoryFormat);

        System.out.println();
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayError() {
        System.out.println("Error: unknown command");
        System.out.println();
    }

}
